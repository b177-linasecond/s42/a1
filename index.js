const txtFirstName = document.querySelector('#txt-first-name')
	  txtLastName = document.querySelector('#txt-last-name')
	  spanFullName = document.querySelector('#span-full-name')

// Alternative ways to retrieve elements
// document.getElementById('txt-first-name')
// document.getElementsByClassName('txt-inputs')
// document.getElementsByTagName('input')

txtFirstName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value
})

txtLastName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value
})